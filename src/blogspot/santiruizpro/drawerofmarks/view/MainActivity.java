/*
 * MainActivity.java
 * 
 * Subjects display screen.
 * 
 * Copyright 2013 Santi Ruiz Andrés <starfly1570@gmail.com>
 * 
 * This file is part of Drawer of Marks.
 * 
 * Drawer of Marks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Drawer of Marks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Drawer of Marks.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package blogspot.santiruizpro.drawerofmarks.view;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.service.dreams.DreamService;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import blogspot.santiruizpro.drawerofmarks.R;
import blogspot.santiruizpro.drawerofmarks.controller.Controller;
import blogspot.santiruizpro.drawerofmarks.model.Subject;

/**
 * @author starfly1570
 */
public class MainActivity extends Activity implements OnItemClickListener,
		OnClickListener, OnItemLongClickListener {

	public static Controller controller;
	private ArrayList<Subject> drawers;
	private View subjectView;
	private int subjectToRemove;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		if (controller == null)
			controller = new Controller(this);

		Button create = (Button) findViewById(R.id.button_create_entity);
		create.setOnClickListener(this);

		Button delete = (Button) findViewById(R.id.button_delete_entity);
		delete.setOnClickListener(this);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == 1) {
			if (resultCode == RESULT_OK) {
				controller.resetDB();
				onResume();
			}
		} else if (requestCode == 2) {
			if (resultCode == RESULT_OK) {
				controller.exportDataBase();
				onResume();
				String text = getResources().getString(R.string.exported);
				Toast.makeText(this, text, Toast.LENGTH_LONG).show();
			}
		} else if (requestCode == 3) {
			if (resultCode == RESULT_OK) {
				Intent intent = new Intent(this,ConfirmationActivity.class);
				intent.putExtra("msg", getResources().getString(R.string.are_you_sure_subject) + " " + drawers.get(subjectToRemove).getName() + "?");
				intent.putExtra("OPTION1", getResources().getString(R.string.yes));
				intent.putExtra("OPTION2", getResources().getString(R.string.no));
				startActivityForResult(intent, 4);
			} else {
				Subject subject = drawers.get(subjectToRemove);
				subject.toggleActive();
				refreshItemBackground(subjectView,subject.isActive());
				controller.updateSubjectActivation(subject.getId(),subject.isActive());
			}
		} else if (requestCode == 4) {
			if (resultCode == RESULT_OK) {
				MainActivity.controller.removeSubject(drawers.get(subjectToRemove).getId());
			}
		}
	}

	@Override
	protected void onResume() {
		super.onResume();

		drawers = controller.getAllSubjects();
		
		TextView info = (TextView) findViewById(R.id.info);
		String subjectsLiteral = getResources().getString(R.string.subjects_info);
		
		Float average = getGlobalAverage();
		
		if ( Double.isNaN(Double.parseDouble(average + "")) ) {
			info.setText(subjectsLiteral + ": ----");
		} else {
			info.setText(subjectsLiteral + ": " + average);
		}
		
		ListView lv = (ListView) findViewById(R.id.list_view);
		DrawerAdapter da = new DrawerAdapter(getLayoutInflater(), drawers,
				getResources());
		lv.setAdapter(da);
		lv.setOnItemClickListener(this);
		lv.setOnItemLongClickListener(this);
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		Intent intent = new Intent(this, SubjectActivity.class);
		intent.putExtra("subject_id", drawers.get(arg2).getId());

		startActivity(intent);
	}
	
	@Override
	public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int arg2,
			long arg3) {

		subjectView = arg1;
		subjectToRemove = arg2;
		Intent intent = new Intent(this,ConfirmationActivity.class);
		intent.putExtra("msg", getResources().getString(R.string.what_do_subject) + " " + drawers.get(arg2).getName() + "?");
		intent.putExtra("OPTION1", getResources().getString(R.string.remove));
		intent.putExtra("OPTION2", getResources().getString(R.string.toggle_activation));
		startActivityForResult(intent, 3);
		onResume();
		return true;
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		if (id == R.id.button_create_entity) {
			Intent intent = new Intent(this, EditActivity.class);
			intent.putExtra("msg",
					getResources().getString(R.string.create_subject));
			intent.putExtra("weightable", true);
			intent.putExtra("weight_mark", false);
			intent.putExtra("mark", false);
			startActivity(intent);
		} else if (id == R.id.button_delete_entity) {
			Intent intent2 = new Intent(this, ConfirmationActivity.class);
			intent2.putExtra("msg",
					getResources().getString(R.string.are_you_sure_all));
			intent2.putExtra("OPTION1", getResources().getString(R.string.yes));
			intent2.putExtra("OPTION2", getResources().getString(R.string.no));
			startActivityForResult(intent2, 1);
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_export) {
			if ( !controller.exportFileExists() ) {
				controller.exportDataBase();
				String text = getResources().getString(R.string.exported);
				Toast.makeText(this, text, Toast.LENGTH_LONG).show();
			} else {
				Intent intent = new Intent(this, ConfirmationActivity.class);
				intent.putExtra("msg",getResources().getString(R.string.export_confirmation));
				intent.putExtra("OPTION1", getResources().getString(R.string.yes));
				intent.putExtra("OPTION2", getResources().getString(R.string.no));
				startActivityForResult(intent, 2);
			}
			return true;
		} else if ( id == R.id.action_import ) {
			if ( !controller.exportFileExists() ) {
				String text = getResources().getString(R.string.export_file_error);
				Toast.makeText(this, text, Toast.LENGTH_LONG).show();
			} else {
				controller.importMarksPackage();
				onResume();
				String text = getResources().getString(R.string.imported);
				Toast.makeText(this, text, Toast.LENGTH_LONG).show();
			}
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private float getGlobalAverage() {
		float average = 0;
		int drawerCount = 0;
		
		for ( int i = 0; i < drawers.size(); i++ ) {
			if ( drawers.get(i).isActive() ) {
				average += drawers.get(i).getAverage();
				drawerCount++;
			}
		}
		
		return average / drawerCount;
	}
	
	private void refreshItemBackground(View v, boolean active) {
		Drawable image = null;
		if ( active ) {
			image = getResources().getDrawable(R.drawable.item_background_color);
		} else {
			image = getResources().getDrawable(R.drawable.item_disabled);
		}
		
		v.setBackgroundDrawable(image);
	}
	
}
