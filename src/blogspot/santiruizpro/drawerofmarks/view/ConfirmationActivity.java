/*
 * ConfirmationActivity.java
 * 
 * The confirmation dialog window.
 * 
 * Copyright 2013 Santi Ruiz Andrés <starfly1570@gmail.com>
 * 
 * This file is part of Drawer of Marks.
 * 
 * Drawer of Marks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Drawer of Marks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Drawer of Marks.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package blogspot.santiruizpro.drawerofmarks.view;

import blogspot.santiruizpro.drawerofmarks.R;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

/**
 * @author starfly1570
 */
public class ConfirmationActivity extends Activity implements OnClickListener {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_confirmation);
		
		Intent intent = getIntent();
		String msg = intent.getStringExtra("msg");
		String opt1 = intent.getStringExtra("OPTION1");
		String opt2 = intent.getStringExtra("OPTION2");
		
		TextView info = (TextView) findViewById(R.id.message);
		info.setText(msg);
		
		Button yes = (Button) findViewById(R.id.yes);
		yes.setText(opt1);
		yes.setOnClickListener(this);
				
		Button no = (Button) findViewById(R.id.no);
		no.setText(opt2);
		no.setOnClickListener(this);
		
	}

	@Override
	public void onClick(View v) {
		
		if (v.getId() == R.id.yes) {
			Intent intent = new Intent();
			setResult(RESULT_OK,intent);
		} else if (v.getId() == R.id.no ){
			Intent intent = new Intent();
			setResult(RESULT_CANCELED,intent);
		}
		
		finish();
	}

}
