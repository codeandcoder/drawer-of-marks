/*
 * EditActivity.java
 * 
 * The drawers creation screen.
 * 
 * Copyright 2013 Santi Ruiz Andrés <starfly1570@gmail.com>
 * 
 * This file is part of Drawer of Marks.
 * 
 * Drawer of Marks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Drawer of Marks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Drawer of Marks.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package blogspot.santiruizpro.drawerofmarks.view;

import blogspot.santiruizpro.drawerofmarks.R;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/**
 * @author starfly1570
 */
public class EditActivity extends Activity implements OnClickListener {

	public static final int DATATYPE_MARK = 10, DATATYPE_WEIGHT = 11,
			DATATYPE_STRING = 12;
	private TextView info;
	private EditText name;
	private EditText weightMark;
	private EditText mark;
	private CheckBox isWeighted;
	private int parentID;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_edit);

		Intent intent = getIntent();
		parentID = intent.getIntExtra("parent_id", -1);
		String message = intent.getStringExtra("msg");
		boolean showWeightable = intent.getBooleanExtra("weightable", false);
		boolean showWeightMark = intent.getBooleanExtra("weight_mark", false);
		boolean showMark = intent.getBooleanExtra("mark", false);

		info = (TextView) findViewById(R.id.message_label);
		info.setText(message);

		name = (EditText) findViewById(R.id.name_field);
		weightMark = (EditText) findViewById(R.id.weight_mark);
		mark = (EditText) findViewById(R.id.mark);
		isWeighted = (CheckBox) findViewById(R.id.is_weighted);
		Button done = (Button) findViewById(R.id.done);
		done.setOnClickListener(this);

		if (!showWeightable)
			isWeighted.setVisibility(View.GONE);

		if (!showWeightMark)
			weightMark.setVisibility(View.GONE);

		if (!showMark)
			mark.setVisibility(View.GONE);

	}

	@Override
	public void onClick(View v) {

		boolean done = false;

		int nameField = R.string.name;
		int markField = R.string.mark;
		int weightField = R.string.weight;

		if (info.getText().equals(
				getResources().getString(R.string.create_subject))) {
			String subject = name.getText().toString().trim();
			if (validateData(subject, DATATYPE_STRING, nameField)) {
				boolean w = isWeighted.isChecked();
				MainActivity.controller.addSubject(subject, w);
				done = true;
			}
		} else if (info.getText().equals(
				getResources().getString(R.string.create_division))) {
			String div = name.getText().toString().trim();
			String weight = weightMark.getVisibility() == View.GONE ? "0.0"
					: weightMark.getText().toString();
			if (validateData(div, DATATYPE_STRING, nameField)
					&& validateData(weight, DATATYPE_WEIGHT, weightField)) {
				boolean w = isWeighted.isChecked();
				MainActivity.controller.addDivision(div, w,
						Float.parseFloat(weight), parentID);
				done = true;
			}
		} else if (info.getText().equals(
				getResources().getString(R.string.create_exam))) {
			String exam = name.getText().toString().trim();
			String weight = weightMark.getVisibility() == View.GONE ? "0.0"
					: weightMark.getText().toString();
			String m = mark.getText().toString();

			if (validateData(exam, DATATYPE_STRING, nameField)
					&& validateData(m, DATATYPE_MARK, markField)
					&& validateData(weight, DATATYPE_WEIGHT, weightField)) {

				MainActivity.controller.addExam(exam, Float.parseFloat(weight),
						Float.parseFloat(m), parentID);
				done = true;
			}
		}

		if (done)
			finish();
	}

	private boolean validateData(String data, int type, int field) {

		String errorMessage = null;
		String fieldName = getResources().getString(field);

		if (data.trim().equals("")) {
			errorMessage = completeMessage(R.string.error_void_field,
					new String[] { fieldName });
		} else {

			switch (type) {
			case DATATYPE_MARK:
				Double mark = Double.parseDouble(data);
				if (mark < 0 || mark > 10)
					errorMessage = completeMessage(R.string.error_mark_format,
							new String[] { fieldName });
				break;
			case DATATYPE_WEIGHT:
				Double weight = Double.parseDouble(data);
				if (weight < 0 || weight > 100)
					errorMessage = completeMessage(
							R.string.error_weight_format,
							new String[] { fieldName });
				break;
			}
		}
		
		if (errorMessage != null)
			Toast.makeText(this, errorMessage, Toast.LENGTH_LONG).show();

		return errorMessage == null;
	}

	private String completeMessage(int message, String[] params) {

		String completeMessage = getResources().getString(message);

		for (int i = 0; i < params.length; i++) {
			completeMessage = completeMessage.replaceFirst("\\?",
					params[i].trim());
		}

		return completeMessage;
	}

}
