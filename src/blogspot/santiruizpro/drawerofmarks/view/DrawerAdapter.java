/*
 * DrawerAdapter.java
 * 
 * The adapter between drawers info and views.
 * 
 * Copyright 2013 Santi Ruiz Andrés <starfly1570@gmail.com>
 * 
 * This file is part of Drawer of Marks.
 * 
 * Drawer of Marks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Drawer of Marks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Drawer of Marks.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package blogspot.santiruizpro.drawerofmarks.view;
import java.util.ArrayList;

import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import blogspot.santiruizpro.drawerofmarks.R;
import blogspot.santiruizpro.drawerofmarks.model.Division;
import blogspot.santiruizpro.drawerofmarks.model.Drawer;
import blogspot.santiruizpro.drawerofmarks.model.Exam;
import blogspot.santiruizpro.drawerofmarks.model.Subject;

/**
 * @author starfly1570
 */
public class DrawerAdapter extends BaseAdapter {

	private ArrayList<Drawer> drawers;
	private LayoutInflater li;
	private Resources resources;

	public DrawerAdapter(LayoutInflater li, ArrayList drawers, Resources res) {
		this.li = li;
		this.drawers = drawers;
		this.resources = res;
	}

	@Override
	public int getCount() {
		return drawers.size();
	}

	@Override
	public Object getItem(int position) {
		return drawers.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder vh;
		Drawer s = drawers.get(position);

		if (convertView == null) {
			convertView = li.inflate(R.layout.item_layout, parent, false);
			vh = new ViewHolder(convertView);
			convertView.setTag(vh);
		} else {
			vh = (ViewHolder) convertView.getTag();
		}

		boolean isActive = false;
		if ( s instanceof Exam ) {
			isActive = ((Exam) s).isActive();
		} else if ( s instanceof Division ) {
			isActive = ((Division) s).isActive();
		} else if ( s instanceof Subject ) {
			isActive = ((Subject) s).isActive();
		}
		
		refreshItemBackground(vh.wholeView, isActive);
		
		float average = s.getAverage();
		
		vh.subject.setText(s.getName());
		vh.markName.setText(numToStringMark(average));
		vh.markName.setTextColor(numToColoredMark(average));
		String format = average == 10 ? "%.1f" : "%.2f";
		if ( Double.isNaN(Double.parseDouble(average + "")) ) {
			vh.mark.setText("----");
		} else {
			vh.mark.setText(String.format(format, average));
		}
		
		return convertView;
	}
	
	private String numToStringMark(float mark) {
		String result = "11";
		if (mark >= 9) {
			result = resources.getString(R.string.markname_excellent);
		} else if (mark >= 7.5) {
			result = resources.getString(R.string.markname_awesome);
		} else if (mark >= 6) {
			result = resources.getString(R.string.markname_verygood);
		} else if (mark >= 5) {
			result = resources.getString(R.string.markname_good);
		} else if (mark >= 2.5) {
			result = resources.getString(R.string.markname_bad);
		} else if (mark >= 0) {
			result = resources.getString(R.string.markname_fail);
		}
		
		return result;
	}
	
	private int numToColoredMark(float mark) {
		int result = 0;
		if (mark >= 9) {
			result = Color.GREEN;
		} else if (mark >= 7.5) {
			result = Color.rgb(180, 255, 180);
		} else if (mark >= 6) {
			result = Color.CYAN;
		} else if (mark >= 5) {
			result = Color.BLUE;
		} else if (mark >= 2.5) {
			result = Color.rgb(255, 180, 180);
		} else if (mark >= 0) {
			result = Color.RED;
		}
		
		return result;
	}
	
	private static class ViewHolder {
		
		View wholeView;
		TextView subject;
		TextView markName;
		TextView mark;

		public ViewHolder(View view) {
			this.wholeView = view;
			this.subject = (TextView) view.findViewById(R.id.subject_name);
			this.markName = (TextView) view.findViewById(R.id.mark_name);
			this.mark = (TextView) view.findViewById(R.id.mark_value);
		}
	}
	
	private void refreshItemBackground(View v, boolean active) {
		Drawable image = null;
		if ( active ) {
			image = resources.getDrawable(R.drawable.item_background_color);
		} else {
			image = resources.getDrawable(R.drawable.item_disabled);
		}
		
		v.setBackgroundDrawable(image);
	}

}
