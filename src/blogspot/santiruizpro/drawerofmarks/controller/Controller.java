/*
 * Controller.java
 * 
 * The application controller.
 * 
 * Copyright 2013 Santi Ruiz Andrés <starfly1570@gmail.com>
 * 
 * This file is part of Drawer of Marks.
 * 
 * Drawer of Marks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Drawer of Marks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Drawer of Marks.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package blogspot.santiruizpro.drawerofmarks.controller;

import java.util.ArrayList;

import android.content.Context;
import blogspot.santiruizpro.drawerofmarks.model.DAO;
import blogspot.santiruizpro.drawerofmarks.model.Division;
import blogspot.santiruizpro.drawerofmarks.model.Subject;
/**
 * @author starfly1570
 */
public class Controller {

	private DAO dao;
	
	public Controller(Context context) {
		dao = new DAO(context);
	}
	
	/**
	 * Search for a subject using its id.
	 * 
	 * @param id the primary key
	 * @return a subject
	 */
	public Subject getSubject(int id) {
		Subject sub = null;
		sub = dao.getSubject(id);
		return sub;
	}
	
	/**
	 * Search for a division using its id.
	 * 
	 * @param id the primary key
	 * @return a division
	 */
	public Division getDivision(int id) {
		Division div = null;
		div = dao.getDivision(id);
		return div;
	}
	
	/**
	 * Returns all stored subjects.
	 * 
	 * @return a list of subjects
	 */
	public ArrayList<Subject> getAllSubjects() {
		ArrayList<Subject> sub = null;
		sub = dao.getSubjects();
		return sub;
	}
	
	/**
	 * Stores a subject.
	 * 
	 * @param subject the name of subject
	 * @param isWeighted true if it is a weighted subject, false otherwise
	 */
	public void addSubject(String subject, boolean isWeighted) {
		dao.addSubject(subject,isWeighted);
	}
	
	/**
	 * Stores a division.
	 * 
	 * @param div the name of division
	 * @param isWeighted true if it is a weighted division, false otherwise
	 * @param weight % of weighting related to containing subject
	 * @param subID id of containing subject
	 */
	public void addDivision(String div, boolean isWeighted, float weight, int subID) {
		dao.addDivision(div, isWeighted, weight, subID);
	}
	
	/**
	 * Stores an exam.
	 * 
	 * @param name the name of exam
	 * @param weight % of weighting related to containing division
	 * @param mark the mark of the exam
	 * @param divID id of containing division
	 */
	public void addExam(String name, float weight, float mark, int divID) {
		dao.addExam(name, mark, weight, divID);
	}
	
	/**
	 * Removes a subject.
	 * 
	 * @param id the subject id
	 */
	public void removeSubject(int id) {
		dao.removeSubject(id);
	}
	
	/**
	 * Removes a division.
	 * 
	 * @param id the division id
	 */
	public void removeDivision(int id) {
		dao.removeDivision(id);
	}
	
	/**
	 * Removes an exam.
	 * 
	 * @param id the exam id
	 */
	public void removeExam(int id) {
		dao.removeExam(id);
	}
	
	/**
	 * Removes all data.
	 */
	public void resetDB() {
		dao.resetDataBase();
	}
	
	/**
	 * Sets the activation field of an exam.
	 * 
	 * @param id the exam id
	 * @param active activation state
	 */
	public void updateExamActivation(int id, boolean active) {
		dao.updateExamActivation(id,active);
	}
	
	/**
	 * Sets the activation field of an division.
	 * 
	 * @param id the division id
	 * @param active activation state
	 */
	public void updateDivisionActivation(int id, boolean active) {
		dao.updateDivisionActivation(id,active);
	}
	
	/**
	 * Sets the activation field of an subject.
	 * 
	 * @param id the subject id
	 * @param active activation state
	 */
	public void updateSubjectActivation(int id, boolean active) {
		dao.updateSubjectActivation(id,active);
	}
	
	/**
	 * Opens a marks package and imports all stored data to database.
	 */
	public void importMarksPackage() {
		dao.importMarksPackage();
	}
	
	/**
	 * Saves (and override if it's the case) a marks package in SD Card.
	 */
	public void exportDataBase() {
		dao.exportDataBase();
	}
	
	/**
	 * Looks for a marks package in SD Card.
	 * 
	 * @return true if the file already exists, false otherwise
	 */
	public boolean exportFileExists() {
		return dao.getExportFile().exists();
	}
	
	/**
	 * See if the SD Card is available or not.
	 * 
	 * @return true if SD Card is available, false otherwise
	 */
	public boolean isSDAvailable() {
		return dao.isSDAvailable();
	}
}
