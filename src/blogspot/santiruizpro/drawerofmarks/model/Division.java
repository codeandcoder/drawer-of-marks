/*
 * Division.java
 * 
 * Information of a division.
 * 
 * Copyright 2013 Santi Ruiz Andrés <starfly1570@gmail.com>
 * 
 * This file is part of Drawer of Marks.
 * 
 * Drawer of Marks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Drawer of Marks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Drawer of Marks.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package blogspot.santiruizpro.drawerofmarks.model;

import java.util.ArrayList;

/**
 * @author starfly1570
 */
public class Division implements Drawer {

	private int id;
	private String name;
	private float weight;
	private ArrayList<Exam> exams;
	private boolean isWeighted;
	private boolean isActive;
	
	public Division(int id, String name, ArrayList<Exam> exams, float weight, boolean isWeighted, boolean isActive) {
		this.id = id;
		this.name = name;
		this.exams = exams;
		this.weight = weight;
		this.isWeighted = isWeighted;
		this.isActive = isActive;
	}
	
	public int getId() {
		return this.id;
	}

	@Override
	public String getName() {
		return name;
	}

	public ArrayList<Exam> getExams() {
		return exams;
	}
	
	public boolean isWeighted() {
		return isWeighted;
	}

	@Override
	public float getWeight() {
		return this.weight;
	}
	
	@Override
	public float getAverage() {
		ArrayList<Exam> activeExams = new ArrayList<Exam>();
		for ( Exam e : exams ) {
			if (e.isActive())
				activeExams.add(e);
		}
		return isWeighted ? Mathematics.getWeightedAverage(activeExams) : Mathematics.getArithmeticAverage(activeExams);
	}
	
	@Override
	public String toString() {
		String result = name + "&";
		for ( Exam e : exams ) {
			result += e.toString() + "_";	
		}
		return result;
	}
	
	public boolean isActive() {
		return isActive;
	}
	
	public void toggleActive() {
		this.isActive = !this.isActive;
	}
	
}
