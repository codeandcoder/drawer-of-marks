/*
 * Exam.java
 * 
 * The information of an exam.
 * 
 * Copyright 2013 Santi Ruiz Andrés <starfly1570@gmail.com>
 * 
 * This file is part of Drawer of Marks.
 * 
 * Drawer of Marks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Drawer of Marks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Drawer of Marks.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package blogspot.santiruizpro.drawerofmarks.model;

/**
 * @author starfly1570
 */
public class Exam implements Drawer {

	private int id;
	private String name;
	private float mark;
	private float weight;
	private boolean isActive;
	
	public Exam(int id, String name, float mark, float weight, boolean isActive) {
		this.id = id;
		this.name = name;
		this.mark = mark;
		this.weight = weight;
		this.isActive = isActive;
	}

	public int getId() {
		return this.id;
	}
	
	@Override
	public String getName() {
		return name;
	}

	@Override
	public float getWeight() {
		return weight;
	}

	@Override
	public float getAverage() {
		return mark;
	}
	
	@Override
	public String toString() {
		return name + "/" + mark;
	}

	public boolean isActive() {
		return isActive;
	}
	
	public void toggleActive() {
		this.isActive = !this.isActive;
	}
	
}
