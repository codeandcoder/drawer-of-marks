/*
 * Subject.java
 * 
 * The information of a Subject.
 * 
 * Copyright 2013 Santi Ruiz Andrés <starfly1570@gmail.com>
 * 
 * This file is part of Drawer of Marks.
 * 
 * Drawer of Marks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Drawer of Marks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Drawer of Marks.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package blogspot.santiruizpro.drawerofmarks.model;

import java.util.ArrayList;

/**
 * @author starfly1570
 */
public class Subject implements Drawer {

	private int id;
	private String name;
	private ArrayList<Division> divisions;
	private boolean isWeighted;
	private boolean isActive;
	
	public Subject (int id, String name, ArrayList<Division> divisions, boolean isWeighted, boolean isActive) {
		this.id = id;
		this.name = name;
		this.divisions = divisions;
		this.isWeighted = isWeighted;
		this.isActive = isActive;
	}

	public int getId() {
		return this.id;
	}
	
	@Override
	public String getName() {
		return name;
	}

	public ArrayList<Division> getDivisions() {
		return divisions;
	}

	public boolean isWeighted() {
		return isWeighted;
	}
	
	@Override
	public float getWeight() {
		return 0;
	}

	@Override
	public float getAverage() {
		ArrayList<Division> activeDivisions = new ArrayList<Division>();
		for ( Division d : divisions ) {
			if (d.isActive())
				activeDivisions.add(d);
		}
		return isWeighted ? Mathematics.getWeightedAverage(activeDivisions) : Mathematics.getArithmeticAverage(activeDivisions);
	}
	
	@Override
	public String toString() {
		String result = name + "$";
		for ( Division d : divisions ) {
			result += d.toString() + ":";
		}
		return result;
	}
	
	public static String printSubjects(ArrayList<Subject> subjects) {
		String result = "";
		for (Subject s : subjects) {
			result += s.toString();
		}
		return result;
	}
	
	public boolean isActive() {
		return isActive;
	}
	
	public void toggleActive() {
		this.isActive = !this.isActive;
	}
}
