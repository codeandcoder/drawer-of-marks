/*
 * Mathematics.java
 * 
 * Calculates averages.
 * 
 * Copyright 2013 Santi Ruiz Andrés <starfly1570@gmail.com>
 * 
 * This file is part of Drawer of Marks.
 * 
 * Drawer of Marks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Drawer of Marks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Drawer of Marks.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package blogspot.santiruizpro.drawerofmarks.model;

import java.util.ArrayList;

/**
 * @author starfly1570
 */
public class Mathematics {

	/**
	 * Calculates a weighted average.
	 * 
	 * @param drawers the group of marks
	 * @return the average
	 */
	public static float getWeightedAverage(ArrayList drawers) {
		float avg = 0;
		float avg2 = 0;
		for (int i = 0; i < drawers.size(); i++) {
			Drawer d = (Drawer) drawers.get(i);
			avg += d.getAverage() * d.getWeight();
			avg2 += d.getWeight();
		}
		return avg /= avg2;
	}
	
	/**
	 * Calculates an arithmetic average.
	 * 
	 * @param drawers the group of marks
	 * @return the average
	 */
	public static float getArithmeticAverage(ArrayList drawers) {
		float avg = 0;
		for (int i = 0; i < drawers.size(); i++) {
			Drawer d = (Drawer) drawers.get(i);
			avg += d.getAverage();
		}
		return avg / drawers.size();
	}
	
}
